# Welcome to Bash Repository

This repository has all **shell scripts** created for some purpose. It can be useful for something or only for studying purposes.


# Files

The most implemented file here is the **movies.sh** which has *cases, while, if and for* statements.

## movies.sh
In this file you can do two mainly things.

 1. Rename files
	 In this case, shell needs to have your Torrent Folder (*or any source folder*) declared as environment variable. Then, it will list all files in *for* statement to get them and add a bullet. You'll need to write the desired folder then it will ask what will be the new name. Simple as it is.

2. Move files
	In this case, it has more stuff to do. First, it will list all the folders as a Destination. You'll need to write it. Then, it will list the content of Source folder to select what will be the folder to be moved. You'll need to write it again.
	After doing that, it will call **rclone** to move this file and show the progress of it. When it finishes, it will ask if you want to Delete, Keep the Source folder or stop the procedure. You have to select a letter.
		- **D** for Deletion
		- **K** to Keep as it is
		- **E** to exit and stop the procedure.

This shell is inside a While infinite loop, so whenever you want to finish, you'll need to hit **CTRL+C** to stop it, or press **[ENTER]** to continue.

# Pre-Deploy

You should add `'$DESTINATION'` and `'$SOURCE'` path as environment variable, which is a "WIP" job.