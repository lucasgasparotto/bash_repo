#!/bin/bash
echo "Download Plex Server Update"
mkdir -p /home/lgasparotto/plexupdate;
cd /home/lgasparotto/plexupdate
wget -c $1 --output-document plexupdate.rpm
yum localinstall plexupdate.rpm -y
systemctl start plexmediaserver
rm -rf /home/lgasparotto/plexupdate/*